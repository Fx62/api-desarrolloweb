USE [API]
GO
/****** Object:  Table [dbo].[Cliente]    Script Date: 10/27/2020 4:07:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Cliente](
	[idCliente] [int] NOT NULL,
	[NombreCliente] [varchar](45) NOT NULL,
	[ApellidoCliente] [varchar](45) NOT NULL,
	[NIT] [int] NOT NULL,
	[CategoriaCliente] [varchar](45) NOT NULL,
	[EstadoCliente] [varchar](45) NOT NULL,
	[DireccionCliente] [varchar](45) NOT NULL,
 CONSTRAINT [PK_Cliente] PRIMARY KEY CLUSTERED 
(
	[idCliente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleNotaCredito]    Script Date: 10/27/2020 4:07:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleNotaCredito](
	[idDetalleNC] [int] NOT NULL,
	[NotaCreditoidNC] [int] NOT NULL,
	[DetalleVentasidDetalleVenta] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_DetalleNC] PRIMARY KEY CLUSTERED 
(
	[idDetalleNC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleVenta]    Script Date: 10/27/2020 4:07:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleVenta](
	[idDetalleVenta] [int] NOT NULL,
	[VentasidVenta] [int] NOT NULL,
	[idProducto] [int] NOT NULL,
	[Cantidad] [int] NOT NULL,
	[Precio] [int] NOT NULL,
 CONSTRAINT [PK_DetalleVenta] PRIMARY KEY CLUSTERED 
(
	[idDetalleVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[NotaCredito]    Script Date: 10/27/2020 4:07:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NotaCredito](
	[idNC] [int] NOT NULL,
	[Anulacion] [char](1) NOT NULL,
	[Detalle] [varchar](45) NOT NULL,
	[Descripcion] [varchar](45) NOT NULL,
 CONSTRAINT [PK_NotaCredito] PRIMARY KEY CLUSTERED 
(
	[idNC] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 10/27/2020 4:07:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[idProducto] [int] NOT NULL,
	[NombreProducto] [varchar](45) NOT NULL,
	[PrecioProducto] [decimal](18, 2) NOT NULL,
	[ExistenciaProducto] [int] NOT NULL,
	[DescripcionProducto] [varchar](45) NOT NULL,
	[CodigoProveedor] [int] NOT NULL,
	[Ubicacion] [varchar](45) NOT NULL,
	[FechaIngreso] [varchar](11) NULL,
	[FechaVencimiento] [varchar](11) NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[idProducto] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Venta]    Script Date: 10/27/2020 4:07:29 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Venta](
	[idVenta] [int] NOT NULL,
	[ClientesidCliente] [int] NOT NULL,
	[FechaVenta] [varchar](11) NULL,
	[TipoVenta] [varchar](2) NOT NULL,
	[DetalleProducto] [varchar](45) NOT NULL,
	[TotalVenta] [decimal](18, 2) NOT NULL,
 CONSTRAINT [PK_Venta] PRIMARY KEY CLUSTERED 
(
	[idVenta] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[DetalleNotaCredito]  WITH CHECK ADD  CONSTRAINT [FK_DetalleNC_DetalleVenta] FOREIGN KEY([DetalleVentasidDetalleVenta])
REFERENCES [dbo].[DetalleVenta] ([idDetalleVenta])
GO
ALTER TABLE [dbo].[DetalleNotaCredito] CHECK CONSTRAINT [FK_DetalleNC_DetalleVenta]
GO
ALTER TABLE [dbo].[DetalleNotaCredito]  WITH CHECK ADD  CONSTRAINT [FK_NotaCredito_NotaCredito] FOREIGN KEY([NotaCreditoidNC])
REFERENCES [dbo].[NotaCredito] ([idNC])
GO
ALTER TABLE [dbo].[DetalleNotaCredito] CHECK CONSTRAINT [FK_NotaCredito_NotaCredito]
GO
ALTER TABLE [dbo].[DetalleVenta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleVenta_Producto] FOREIGN KEY([idProducto])
REFERENCES [dbo].[Producto] ([idProducto])
GO
ALTER TABLE [dbo].[DetalleVenta] CHECK CONSTRAINT [FK_DetalleVenta_Producto]
GO
ALTER TABLE [dbo].[DetalleVenta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleVenta_Venta] FOREIGN KEY([VentasidVenta])
REFERENCES [dbo].[Venta] ([idVenta])
GO
ALTER TABLE [dbo].[DetalleVenta] CHECK CONSTRAINT [FK_DetalleVenta_Venta]
GO
ALTER TABLE [dbo].[Venta]  WITH CHECK ADD  CONSTRAINT [FK_Cliente_Venta] FOREIGN KEY([ClientesidCliente])
REFERENCES [dbo].[Cliente] ([idCliente])
GO
ALTER TABLE [dbo].[Venta] CHECK CONSTRAINT [FK_Cliente_Venta]
GO
