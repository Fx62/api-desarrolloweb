﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFinal1.Entities
{
    public class Ventas
    {
        [Key]
        [Display(Name = "Código de Venta")]
        public int idVenta { get; set; }

        [Required]
        [Column(TypeName = "Varchar(11)")]
        [Display(Name = "Fecha de Venta")]
        public string FechaVenta { get; set; }

        [Required]
        [Column(TypeName = "Varchar(2)")]
        [Display(Name = "Tipo de Venta")]
        public string TipoVenta { get; set; } // my => mayor || mn => menor

        public virtual ICollection<DetalleVentas> DetalleVentas { get; set; }

        [Required]
        [Column(TypeName = "decimal(8,2)")]
        [Display(Name = "Total de Venta")]
        public decimal TotalVenta { get; set; }

        //[Required]
        public int ClientesidCliente { get; set; }
        [Required]
        public string DetalleProducto { get; set; }

    }
}
