﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFinal1.Entities
{
    public class DetalleVentas
    {
        [Key]
        [Display(Name = "Código de Detalle Ventas")]
        public int idDetalleVenta { get; set; }

        [Required]
        [Column(TypeName = "int")]
        [Display(Name = "Cantidad Detalle Ventas")]
        public int Cantidad { get; set; } 

        [Required]
        //[Column(TypeName = "decimal(8,2)")]
        [Display(Name = "Precio Detalle Ventas")]
        public int Precio { get; set; }
        
        public int VentasidVenta { get; set; }

        public List<DetalleNotaCreditos> detalleNotaCreditos { get; set; }
        public int idProducto { get; set; }
    }
}
