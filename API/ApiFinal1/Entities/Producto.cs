﻿using ApiFinal1.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFinal.Entities
{
    public class Producto
    {
        [Key]
        
        public int idProducto { get; set; }

        public string NombreProducto { get; set; }

        public decimal PrecioProducto { get; set; }

        public int ExistenciaProducto { get; set; }

        public string DescripcionProducto { get; set; }
        public int CodigoProveedor { get; set; }
        public string Ubicacion { get; set; }
        public string FechaIngreso { get; set; }
        public string FechaVencimiento { get; set; }
        

        //public List<DetalleVentas> DetalleVentas { get; set; }
    }
}
