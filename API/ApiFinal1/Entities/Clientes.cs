﻿using ApiFinal1.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFinal.Entities
{
    public class Clientes
    {
        [Key]
        
        public int idCliente { get; set; }

        public string Nombrecliente { get; set; }

        public string ApellidoCliente { get; set; }

        public int NIT { get; set; }

        public string DireccionCliente { get; set; }

        public string CategoriaCliente { get; set; }

        public string EstadoCliente { get; set; }

        public List<Ventas> Ventas { get; set; }
    }
}
