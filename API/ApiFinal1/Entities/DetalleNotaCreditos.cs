﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFinal1.Entities
{
    public class DetalleNotaCreditos
    {
        [Key]
        public int idDetalleNC { get; set; }
        public int NotaCreditoidNC { get; set; }
        public int DetalleVentasidDetalleVenta { get; set; }
        public int Cantidad { get; set; }
        public decimal Precio { get; set; }
    }
}
