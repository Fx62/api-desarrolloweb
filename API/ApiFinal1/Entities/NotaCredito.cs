﻿using ApiFinal1.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFinal.Entities
{
    public class NotaCredito
    {
        [Key]
        public int idNC { get; set; }
        public char Anulacion { get; set; }
        
        public string Detalle { get; set; }
        public string Descripcion { get; set; }
        public List<DetalleNotaCreditos> detalleNotaCreditos { get; set; }

    }
}
