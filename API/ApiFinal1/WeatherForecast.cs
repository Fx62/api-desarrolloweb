using System;

namespace ApiFinal1
{
    public class WeatherForecast
    {
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }
        public System.Collections.Generic.List<Days> days { get; set; }
    }

    public class Days { 
        public int number { get; set; }
        public string name { get; set; }

    }
}
