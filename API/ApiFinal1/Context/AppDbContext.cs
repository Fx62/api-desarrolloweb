﻿using ApiFinal.Entities;
using ApiFinal1.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiFinal1.Context
{
    public class AppDbContext:DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Ventas> Venta { get; set; }
        public DbSet<DetalleVentas> DetalleVenta { get; set; }
        public DbSet<DetalleNotaCreditos> DetalleNotaCredito { get; set; }
        public DbSet<Clientes> Cliente { get; set; }
        public DbSet<Producto> Producto { get; set; }
        public DbSet<NotaCredito> NotaCredito { get; set; }


        public static implicit operator ControllerContext(AppDbContext v)
        {
            throw new NotImplementedException();
        }
    }
}
