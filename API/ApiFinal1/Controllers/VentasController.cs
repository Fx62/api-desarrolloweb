﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiFinal1.Context;
using ApiFinal1.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ApiFinal1.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class VentasController : ControllerBase
    {
        private readonly AppDbContext context;
        public VentasController(AppDbContext context)
        {
            this.context = context;
        }
        [HttpGet]
        [ActionName("All")]
        public IEnumerable<Ventas> All()
        {
            // *** Ascendente ***
            List<Ventas> ventas = context.Venta.ToList();

            // *** Descendente ***
            //  List<Ventas> ventas = context.Venta.OrderByDescending(v=>v.TotalVenta).ToList();


            //List<Ventas> ventas = context.Venta.ToList();
            foreach (var venta in ventas)
            {
                List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                foreach (var detalleVenta in detalleVentas)
                {
                    detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                }
                venta.DetalleVentas = detalleVentas;
            }
            return ventas;
        }

        // GET: api/<VentasController>
        [HttpGet]
        [ActionName("Menor")]
        public IEnumerable<Ventas> Menor()
        {
            // *** Ascendente ***
            List<Ventas> ventas = context.Venta.OrderBy(v => v.TotalVenta).ToList();

            foreach (var venta in ventas)
            {
                List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                foreach (var detalleVenta in detalleVentas)
                {
                    detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                }
                venta.DetalleVentas = detalleVentas;
            }
            return ventas;
        }

        [HttpGet]
        [ActionName("Mayor")]
        public IEnumerable<Ventas> Mayor()
        {

            // *** Descendente ***
            List<Ventas> ventas = context.Venta.OrderByDescending(v => v.TotalVenta).ToList();

            foreach (var venta in ventas)
            {
                List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                foreach (var detalleVenta in detalleVentas)
                {
                    detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                }
                venta.DetalleVentas = detalleVentas;
            }
            return ventas;
        }

        // GET api/<VentasController>/5
        [HttpGet("{id}")]
        [ActionName("venta")]
        public Ventas venta(int id)
        {
            var ventas = context.Venta.FirstOrDefault(p => p.idVenta == id);
            List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == id).ToList();
            foreach (var detalleVenta in detalleVentas)
            {
                detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
            }
            ventas.DetalleVentas = detalleVentas;
            return ventas;
        }



        // POST api/<VentasController>
        [HttpPost]
        [ActionName("agregar")]
        public ActionResult agregar([FromBody] Ventas ventas)
        {
            try
            {
                context.Venta.Add(ventas);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex);
            }
        }



        /*
        // PUT api/<VentasController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] Ventas ventas)
        {
            if (ventas.idVentas == id)
            {
                context.Entry(ventas).State = EntityState.Modified;
                context.SaveChanges();
                return Ok();
            }
            else {
                return BadRequest();
            }
        }

        // DELETE api/<VentasController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var ventas = context.tbl_Ventas.FirstOrDefault(p => p.idVentas == id);
            if (ventas != null)
            {
                context.tbl_Ventas.Remove(ventas);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
        */
    }
}
