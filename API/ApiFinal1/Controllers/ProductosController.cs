﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApiFinal.Entities;
using ApiFinal1.Context;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiFinal.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ProductosController : Controller
    {
        private readonly AppDbContext context;
        public ProductosController(AppDbContext context)
        {
            this.context = context;
        }

        // GET: api/<ClientesController>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Producto> all()
        {
            return context.Producto.ToList();
        }

        // GET api/<ClientesController>/5
        [HttpGet("{id}")]
        [ActionName("producto")]
        public Producto producto(int id)
        {
            var producto = context.Producto.FirstOrDefault(p => p.idProducto == id);
            return producto;
        }

        [HttpGet("{date}")]
        [ActionName("vencimiento")]
        public List<Producto> vencimiento(string date)
        {
            return context.Producto.Where(p => p.FechaVencimiento == date).ToList();
        }

        [HttpGet("{descripcion}")]
        [ActionName("descripcion")]
        public List<Producto> descripcion(string descripcion)
        {
            return context.Producto.Where(p=>p.DescripcionProducto.Contains(descripcion)).ToList();
        }

        // POST api/<ClientesController>
        [HttpPost]
        [ActionName("agregar")]
        public ActionResult agregar([FromBody] Producto producto)
        {
            try
            {
                context.Producto.Add(producto);
                context.SaveChanges();
                return Ok();
            } catch(Exception e)
            {
                return BadRequest();
            }
        }

        // PUT api/<ClientesController>/5
        [HttpPut("{id}")]
        [ActionName("actualizar")]
        public ActionResult actualizar(int id, [FromBody] Producto producto)
        {
            if(producto.idProducto == id)
            {
                context.Entry(producto).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                context.SaveChanges();
                return Ok();
            } else
            {
                return BadRequest();
            }
        }

        // DELETE api/<ClientesController>/5
        [HttpDelete("{id}")]
        [ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            var producto = context.Producto.FirstOrDefault(p => p.idProducto == id);
            if (producto != null)
            {
                context.Producto.Remove(producto);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
