﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiFinal1.Context;
using ApiFinal1.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace ApiFinal1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetalleNotaCreditosController : ControllerBase
    {
        private readonly AppDbContext context;
        public DetalleNotaCreditosController(AppDbContext context)
        {
            this.context = context;
        }

        [HttpGet]
        public IEnumerable<DetalleNotaCreditos> Get()
        {
            List<DetalleNotaCreditos> detalleNotaCreditos = context.DetalleNotaCredito.ToList();
            return detalleNotaCreditos;
        }

        [HttpGet("{id}")]
        public DetalleNotaCreditos Get(int id)
        {
            var detalleNotaCreditos = context.DetalleNotaCredito.FirstOrDefault(p => p.idDetalleNC == id);
            return detalleNotaCreditos;
        }


        [HttpPost]
        public ActionResult Post([FromBody] DetalleNotaCreditos detalleNotaCreditos)
        {
            try
            {
                context.DetalleNotaCredito.Add(detalleNotaCreditos);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }
    }
}
