﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApiFinal.Entities;
using ApiFinal1.Context;
using ApiFinal1.Entities;
using Microsoft.AspNetCore.Mvc;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiFinal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class NotaCreditosController : Controller
    {
        private readonly AppDbContext context;
        public NotaCreditosController(AppDbContext context)
        {
            this.context = context;
        }

        // GET: api/<ClientesController>
        [HttpGet]
        public IEnumerable<NotaCredito> Get()
        {
            List<NotaCredito> notaCreditos =  context.NotaCredito.ToList();
            foreach(var notaCredito in notaCreditos)
            {
                List<DetalleNotaCreditos> detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.NotaCreditoidNC == notaCredito.idNC).ToList();
                notaCredito.detalleNotaCreditos = detalleNotaCreditos;
            }
            return notaCreditos;
        }

        // GET api/<ClientesController>/5
        [HttpGet("{id}")]
        public NotaCredito Get(int id)
        {
            var notaCredito = context.NotaCredito.FirstOrDefault(p => p.idNC == id);
            notaCredito.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.NotaCreditoidNC == notaCredito.idNC).ToList();
            return notaCredito;
        }

        // POST api/<ClientesController>
        [HttpPost]
        public ActionResult Post([FromBody] NotaCredito notaCredito)
        {
            try
            {
                context.NotaCredito.Add(notaCredito);
                context.SaveChanges();
                return Ok();
            } catch(Exception e)
            {
                return BadRequest();
            }
        }

        /*
        // PUT api/<ClientesController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] NotaCredito notaCredito)
        {
            if(notaCredito.idNC == id)
            {
                context.Entry(notaCredito).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                context.SaveChanges();
                return Ok();
            } else
            {
                return BadRequest();
            }
        }

        // DELETE api/<ClientesController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var notaCredito = context.NotaCredito.FirstOrDefault(p => p.idNC == id);
            if (notaCredito != null)
            {
                context.NotaCredito.Remove(notaCredito);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        */
    }
}
