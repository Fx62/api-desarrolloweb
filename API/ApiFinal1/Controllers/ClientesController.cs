﻿using System;
using System.Collections.Generic;
using System.Linq;
using ApiFinal.Entities;
using ApiFinal1.Context;
using ApiFinal1.Entities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace ApiFinal.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ClientesController : Controller
    {
        private readonly AppDbContext context;
        public ClientesController(AppDbContext context)
        {
            this.context = context;
        }

        // GET: api/<ClientesController>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Clientes> all()
        {
            List<Clientes> clientes = context.Cliente.ToList();
            foreach (var cliente in clientes)
            {
                List<ApiFinal1.Entities.Ventas> ventas = context.Venta.Where(p => p.ClientesidCliente == cliente.idCliente).ToList();

                foreach (var venta in ventas)
                {
                    List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                    foreach (var detalleVenta in detalleVentas) {
                        detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                    }
                    venta.DetalleVentas = detalleVentas;
                }
                cliente.Ventas = ventas;
            }
            return clientes;
        }

        // GET api/<ClientesController>/5
        [HttpGet("{id}")]
        [ActionName("cliente")]
        public List<Clientes> cliente(int id)
        {
            List<Clientes> clientes = context.Cliente.Where(p => p.idCliente == id).ToList();
            
            foreach (var cliente in clientes)
            {
                List<ApiFinal1.Entities.Ventas> ventas = context.Venta.Where(p => p.ClientesidCliente == cliente.idCliente).ToList();
                foreach (var venta in ventas)
                {
                    List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                    foreach (var detalleVenta in detalleVentas)
                    {
                        detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                    }
                    venta.DetalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                }
                cliente.Ventas = ventas;
            }
           
            return clientes;
        }

        // GET api/<ClientesController>/5
        [HttpGet("{Nombre}")]
        [ActionName("nombre")]
        public List<Clientes> nombre(string nombre)
        {
            List<Clientes> clientes = context.Cliente.Where(p => p.Nombrecliente == nombre).ToList();
            foreach (var cliente in clientes)
            {
                List<ApiFinal1.Entities.Ventas> ventas = context.Venta.Where(p => p.ClientesidCliente == cliente.idCliente).ToList();
                foreach (var venta in ventas)
                {
                    List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                    foreach (var detalleVenta in detalleVentas)
                    {
                        detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                    }
                    venta.DetalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                }
                cliente.Ventas = ventas;
            }

            return clientes;
        }

        // GET api/<ClientesController>/5
        [HttpGet("{Apellido}")]
        [ActionName("Apellido")]
        public List<Clientes> Apellido(string apellido)
        {
            List<Clientes> clientes = context.Cliente.Where(p => p.ApellidoCliente == apellido).ToList();
            foreach (var cliente in clientes)
            {
                List<ApiFinal1.Entities.Ventas> ventas = context.Venta.Where(p => p.ClientesidCliente == cliente.idCliente).ToList();
                foreach (var venta in ventas)
                {
                    List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                    foreach (var detalleVenta in detalleVentas)
                    {
                        detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                    }
                    venta.DetalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                }
                cliente.Ventas = ventas;
            }

            return clientes;
        }

        // GET api/<ClientesController>/5
        [HttpGet("{NIT}")]
        [ActionName("NIT")]
        public List<Clientes> NIT(int nit)
        {
            List<Clientes> clientes = context.Cliente.Where(p => p.NIT == nit).ToList();
            foreach (var cliente in clientes)
            {
                List<ApiFinal1.Entities.Ventas> ventas = context.Venta.Where(p => p.ClientesidCliente == cliente.idCliente).ToList();
                foreach (var venta in ventas)
                {
                    List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                    foreach (var detalleVenta in detalleVentas)
                    {
                        detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                    }
                    venta.DetalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                }
                cliente.Ventas = ventas;
            }

            return clientes;
        }

        // GET api/<ClientesController>/5
        [HttpGet("{Estado}")]
        [ActionName("Estado")]
        public List<Clientes> Estado(string estado)
        {
            List<Clientes> clientes = context.Cliente.Where(p => p.EstadoCliente == estado).ToList();
            foreach (var cliente in clientes)
            {
                List<ApiFinal1.Entities.Ventas> ventas = context.Venta.Where(p => p.ClientesidCliente == cliente.idCliente).ToList();
                foreach (var venta in ventas)
                {
                    List<DetalleVentas> detalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                    foreach (var detalleVenta in detalleVentas)
                    {
                        detalleVenta.detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                    }
                    venta.DetalleVentas = context.DetalleVenta.Where(p => p.idDetalleVenta == venta.idVenta).ToList();
                }
                cliente.Ventas = ventas;
            }

            return clientes;
        }

        // POST api/<ClientesController>
        [HttpPost]
        [ActionName("agregar")]
        public ActionResult agregar([FromBody] Clientes clientes)
        {
            try
            {
                context.Cliente.Add(clientes);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        // PUT api/<ClientesController>/5
        [HttpPut("{id}")]
        [ActionName("actualizar")]
        public ActionResult actualizar(int id, [FromBody] Clientes clientes)
        {
            if (clientes.idCliente == id)
            {
                context.Entry(clientes).State = EntityState.Modified;
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE api/<ClientesController>/5
        [HttpDelete("{id}")]
        [ActionName("Delete")]
        public ActionResult Delete(int id)
        {
            var clientes = context.Cliente.FirstOrDefault(p => p.idCliente == id);
            if (clientes != null)
            {
                try
                {
                    context.Cliente.Remove(clientes);
                    context.SaveChanges();
                    return Ok();
                } catch(Exception e)
                {
                    return BadRequest("No se puede borrar porque el usuario tiene registros de ventas");
                }
            }
            else
            {
                return BadRequest();
            }
        }
    }
}
