﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiFinal1.Context;
using ApiFinal1.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace ApiFinal1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetalleVentasController : ControllerBase
    {
        private readonly AppDbContext context;
        public DetalleVentasController(AppDbContext context)
        {
            this.context = context;
        }

        // GET: api/<DetalleVentasController>
        [HttpGet]
        public IEnumerable<DetalleVentas> Get()
        {
            List<DetalleVentas> detalleVentas = context.DetalleVenta.ToList();
            foreach(var detalleVenta in detalleVentas)
            {
                List<DetalleNotaCreditos> detalleNotaCreditos = context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == detalleVenta.idDetalleVenta).ToList();
                detalleVenta.detalleNotaCreditos = detalleNotaCreditos;
            }
            return detalleVentas;
        }

        // GET api/<DetalleVentasController>/5
        [HttpGet("{id}")]
        public DetalleVentas Get(int id)
        {
            var dVentas = context.DetalleVenta.FirstOrDefault(p => p.idDetalleVenta == id);
            context.DetalleNotaCredito.Where(d => d.DetalleVentasidDetalleVenta == dVentas.idDetalleVenta).ToList();
            return dVentas;
        }



        // POST api/<DetalleVentasController>
        [HttpPost]
        public ActionResult Post([FromBody] DetalleVentas detalleVentas)
        {
            try
            {
                context.DetalleVenta.Add(detalleVentas);
                context.SaveChanges();
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest();
            }
        }

        /*
        // PUT api/<DetalleVentasController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] DetalleVentas detalleVentas)
        {
            if (detalleVentas.idDetalleVentas == id)
            {
                context.Entry(detalleVentas).State = EntityState.Modified;
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }

        // DELETE api/<DetalleVentasController>/5
        [HttpDelete("{id}")]
        public ActionResult Delete(int id)
        {
            var dVentas = context.DetalleVentas.FirstOrDefault(p => p.idDetalleVentas == id);
            if (dVentas != null)
            {
                context.DetalleVentas.Remove(dVentas);
                context.SaveChanges();
                return Ok();
            }
            else
            {
                return BadRequest();
            }
        }
        */
    }
}
